package com.fisa.curso.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fisa.curso.modelo.EstadoCivil;
import com.fisa.curso.modelo.Genero;
import com.fisa.curso.modelo.Persona;
import com.fisa.curso.negocio.PersonaService;

@SuppressWarnings("serial")
//@WebServlet("/personaController")
public class PersonaController extends HttpServlet {

	//private static final long serialVersionUID = 1L;

	@Inject
	PersonaService personaService;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		service(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		service(req, resp);
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String identificacion = req.getParameter("identificacion");
		String nombre = req.getParameter("nombre");
		String apellido = req.getParameter("apellido");
		String genero = req.getParameter("genero");
		String estadoCivil = req.getParameter("estadoCivil");

		Persona persona = new Persona();
		persona.setIdentificacion(identificacion);
		persona.setApellido(apellido);
		persona.setNombre(nombre);
		persona.setEstadoCivil(EstadoCivil.valueOf(estadoCivil));
		persona.setGenero(Genero.valueOf(genero));
		
		
		//Persona persona = (Persona) req.getAttribute("persona");
		
		PrintWriter out = resp.getWriter();
		try {
			personaService.create(persona);
			//out.println("<h3>Se guardo la persona</h3>");
			RequestDispatcher rd = req.getRequestDispatcher("listaPersonas");
			//int a = 5 / 0;
			rd.forward(req, resp);
		} catch (Exception e) {
			String urlToRedirect = resp.encodeRedirectURL("error.jsp?mensaje="+e.getMessage());
			resp.sendRedirect(urlToRedirect);
			//out.println("<h3>No se pudo guardar la persona</h3>");
			
		}
	}

}
