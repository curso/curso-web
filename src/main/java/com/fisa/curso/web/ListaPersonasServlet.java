package com.fisa.curso.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fisa.curso.modelo.Persona;
import com.fisa.curso.negocio.PersonaService;


@WebServlet("/listaPersonas")
public class ListaPersonasServlet extends HttpServlet {

	@Inject
	PersonaService personaService;

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			PrintWriter writer = response.getWriter();
			List<Persona> personas = personaService.retrieve();
			writer.print("<h1>Lista de personas - Servlet version</h1>");
			writer.print("<table>");
			for (Persona p : personas) {
				writer.print("<tr>");
				writer.print("<td>");
				writer.print(p.getIdentificacion());
				writer.print("</td>");

				writer.print("<td>");
				writer.print(p.getApellido());
				writer.print("</td>");

				writer.print("<td>");
				writer.print(p.getNombre());
				writer.print("</td>");

				writer.print("<td>");
				writer.print(p.getGenero());
				writer.print("</td>");

				writer.print("<td>");
				writer.print(p.getEstadoCivil());
				writer.print("</td>");

				writer.print("</tr>");
			}
			writer.print("</table>");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doGet(req, resp);
	}
}
