package com.fisa.curso.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * Servlet Filter implementation class TimeFilter
 */
//@WebFilter(filterName = "TimeFilter", urlPatterns = { "/*" })
public class RandomFilter implements Filter {

	public void destroy() {

	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		PrintWriter writer = response.getWriter();
		if (Math.random() < 0.5) {
			writer.println("<h3>FILTERED REQUEST APPROVED</h3>");
			chain.doFilter(request, response);
		} else {
			writer.println("<h3>FILTERED REQUEST DENIED</h3>");
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
