<jsp:useBean id="persona" class="com.fisa.curso.modelo.Persona"/>  

<html>
<body>
	<h1>Lista de personas - JSP version</h1>
	<table>
		<thead>
			<tr>
				<th>Identificacion</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Genero</th>
				<th>Estado Civil</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th>Identificacion</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Genero</th>
				<th>Estado Civil</th>
			</tr>
		</tbody>
	</table>
	
</body>
</html>
